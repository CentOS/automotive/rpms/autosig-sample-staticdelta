Name:           autosig-sample-staticdelta
Version:        0.1
Release:        1%{?dist}
Summary:        Tools for handling the autosig runvm external watchdog

License:        MIT
Source0:        static-delta-apply
Source1:        static-delta-info

Requires:       python3
Requires:       python3-gobject
Requires:       rpm-ostree
Requires:       ostree

%description
Tools for handling static delta updates

%prep

%build

%install

mkdir -p %{buildroot}%{_bindir}
install  -m 0755 %{SOURCE0} %{buildroot}%{_bindir}/
install  -m 0755 %{SOURCE1} %{buildroot}%{_bindir}/

%files
%{_bindir}/static-delta-apply
%{_bindir}/static-delta-info

%changelog
* Wed Apr 27 2022 Alexander Larsson <alexl@redhat.com> - 0.1-1
- Initial version
